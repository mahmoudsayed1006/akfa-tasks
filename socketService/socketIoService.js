var Message = require('../models/message/message.model');
var MessageController = require('../controllers/message/messageController');

module.exports = {

    startChat : function (io) {
        var nsp = io.of('/chat');

        nsp.on('connection', function (socket) { 
            var myId = socket.handshake.query.id;
            var roomName = 'room-' + myId;
            socket.join(roomName);
            console.log('client '+myId+' connected.') 

            socket.on('newMessage', function (data) {
                console.log(data);
                MessageController.addnewMessage(io,nsp,data);
            });

            // ana hana 3ayza al id bta3 al "to" basss
            socket.on('seen',function(data){
                data.myId = myId;
                console.log("in server in seeen")
                MessageController.updateSeen(nsp,data);

            });


            socket.on('typing', function (data) {
                var toRoom = 'room-' + data.toId;
                nsp.to(toRoom).emit('typing', data);
            });
            
            socket.on('stopTyping', function (data) {
                var toRoom = 'room-' + data.toId;
                nsp.to(toRoom).emit('stopTyping', data);
            });

            socket.on('disconnect', function () {
                console.log('client disconnected');
            });

        });
    }
}