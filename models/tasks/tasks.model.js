import mongoose,{ Schema} from "mongoose";
import autoIncrement from 'mongoose-auto-increment';
import { isImgUrl } from "../../helpers/CheckMethods";

const TaskSchema = new Schema({
    _id: {
        type: Number,
        required: true
    },
    employee:{
        type:Number,
        ref:'user',
        required:true
    },
    task:{
        type:String,
        required:true
    },
    follower:{
        type:Number,
        ref:'user',
        required:true
    },
    department:{
        type:Number,
        ref:'department',
        required:true
    },
    start:{
        required:true,
        type : Date,
        default: Date.now
    },
    end:{
        required:true,
        type : Date,
    },
    details:{
        type:String
    },
    type:{
        type: String,
        enum: ['IMPORTANT','NORMAL','URGENT'],
        default:'NORMAL'
    },
    status:{
        type: String,
        enum: ['WAITING','FINSHED','INPROGRESS','REJECTED'],
        default:'WAITING'
    },
    
    reply:{
        type:String,
    },
    doneDate:{
        type : Date,
    },
    doneImg:{
        type: String,
        
    },
    rate:{
        type:Number,
        default:0
    },
    steps:{
        type:[Number]
    },
    deleted:{
        type:Boolean,
        default:false
    }
}, { timestamps: true });

TaskSchema.set('toJSON', {
    transform: function (doc, ret, options) {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
    }
});
autoIncrement.initialize(mongoose.connection);
TaskSchema.plugin(autoIncrement.plugin, { model: 'task', startAt: 1 });

export default mongoose.model('task', TaskSchema);