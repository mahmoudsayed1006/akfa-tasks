import mongoose,{ Schema} from "mongoose";
import autoIncrement from 'mongoose-auto-increment';
import { isImgUrl } from "../../helpers/CheckMethods";

const SubTaskSchema = new Schema({
    _id: {
        type: Number,
        required: true
    },
    employee:{
        type:Number,
        ref:'user',
        required:true
    },
    task:{
        type:Number,
        ref:'task',
        required:true
    },
    title: {
        type: String,
        required: true
    },
    number: {
        type: Number,
    },
    date: {
        type: Date,
    },
    endDate: {
        type: Date,
        required:true
    },
    notes:{
        type:String
    },
    complete:{
        type:Boolean,
        default:false
    },
    completeImg:{
        type: [String],
        /*validate: {
            validator: imgUrl => isImgUrl(imgUrl),
            message: 'img is invalid url'
        }*/
    },
    deleted:{
        type:Boolean,
        default:false
    }
}, { timestamps: true });

SubTaskSchema.set('toJSON', {
    transform: function (doc, ret, options) {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
    }
});
autoIncrement.initialize(mongoose.connection);
SubTaskSchema.plugin(autoIncrement.plugin, { model: 'subtask', startAt: 1 });

export default mongoose.model('subtask', SubTaskSchema);