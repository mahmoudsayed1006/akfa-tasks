import mongoose,{ Schema} from "mongoose";
import autoIncrement from 'mongoose-auto-increment';
const ArchiveSchema = new Schema({
    _id: {
        type: Number,
        required: true
    },
    description: {
        type: String,
        trim: true,
        required: true,
    },
    files:[{
        type: String,
        required: true,
    }],
    user:{
        type:Number,
        required:true,
        ref:'user'
    },
    deleted:{
        type:Boolean,
        default:false
    }
}, { timestamps: true });

ArchiveSchema.set('toJSON', {
    transform: function (doc, ret, options) {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
    }
});
autoIncrement.initialize(mongoose.connection);
ArchiveSchema.plugin(autoIncrement.plugin, { model: 'archive', startAt: 1 });

export default mongoose.model('archive', ArchiveSchema);