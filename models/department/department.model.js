import mongoose,{ Schema} from "mongoose";
import autoIncrement from 'mongoose-auto-increment';
const DepartmentSchema = new Schema({
    _id: {
        type: Number,
        required: true
    },
    name: {
        type: String,
        trim: true,
        required: true,
    },
    deleted:{
        type:Boolean,
        default:false
    }
}, { timestamps: true });

DepartmentSchema.set('toJSON', {
    transform: function (doc, ret, options) {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
    }
});
autoIncrement.initialize(mongoose.connection);
DepartmentSchema.plugin(autoIncrement.plugin, { model: 'department', startAt: 1 });

export default mongoose.model('department', DepartmentSchema);