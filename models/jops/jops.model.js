import mongoose,{ Schema} from "mongoose";
import autoIncrement from 'mongoose-auto-increment';
const JopSchema = new Schema({
    _id: {
        type: Number,
        required: true
    },
    name: {
        type: String,
        trim: true,
        required: true,
    },
    department:{
        type:Number,
        required: true,
        ref:'department'
    },
    deleted:{
        type:Boolean,
        default:false
    }
}, { timestamps: true });

JopSchema.set('toJSON', {
    transform: function (doc, ret, options) {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
    }
});
autoIncrement.initialize(mongoose.connection);
JopSchema.plugin(autoIncrement.plugin, { model: 'jop', startAt: 1 });

export default mongoose.model('jop', JopSchema);