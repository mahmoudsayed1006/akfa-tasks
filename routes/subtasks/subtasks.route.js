import express from 'express';
import SubTasksController from '../../controllers/subtasks/subtasks.controller';
import {requireAuth } from '../../services/passport';
import { multerSaveTo } from '../../services/multer-service';

const router = express.Router();

router.route('/')
    .post(
        requireAuth,
        
        SubTasksController.create
    )
    .get(SubTasksController.findAll);
    
router.route('/:taskId/tasks')
    .get(SubTasksController.findAllInTask);

router.route('/:subtaskId')
    .get(SubTasksController.findById)
    .delete(requireAuth,SubTasksController.delete);

router.route('/:subtaskId/complete')
    .put(
        requireAuth,
        multerSaveTo('subtasks').array('completeImg',10),
        SubTasksController.validateBody(),
        SubTasksController.complete
    );



export default router;