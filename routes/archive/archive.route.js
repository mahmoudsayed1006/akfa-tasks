import express from 'express';
import ArchiveController from '../../controllers/archive/archive.controller';
import {requireAuth } from '../../services/passport';
import { multerSaveTo } from '../../services/multer-service';

const router = express.Router();

router.route('/')
    .post(
         requireAuth,
         multerSaveTo('archives').array('files',1000),
         ArchiveController.validateBody(),
         ArchiveController.create
    );

router.route('/:userId/users')
    .get( ArchiveController.findAll);

router.route('/:archiveId')
    .put(
        requireAuth,
        multerSaveTo('archives').array('files',1000),
        ArchiveController.validateBody(true),
        ArchiveController.update
    )
    .get( ArchiveController.findById)
    .delete(requireAuth, ArchiveController.delete);

router.route('/search')
    .post(
        requireAuth,
        ArchiveController.search
    );






export default router;