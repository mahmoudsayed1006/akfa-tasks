import express from 'express';
import userRoute from './user/user.route';
import jopRoute from './jops/jops.route';
import departmentRoute from './department/department.route';
import taskRoute from './tasks/tasks.route';
import archiveRoute from './archive/archive.route';
import subtaskRoute from './subtasks/subtasks.route';
import notifRoute from './notif/notif.route';
import { requireAuth } from '../services/passport';
var messageRoute = require('../routes/message/message.route');

const router = express.Router();

router.use('/', userRoute);
router.use('/jops', jopRoute);
router.use('/department', departmentRoute);
router.use('/tasks', taskRoute);
router.use('/archives', archiveRoute);
router.use('/subtasks', subtaskRoute);
router.use('/notif', notifRoute);
router.use('/messages',messageRoute);

export default router;
