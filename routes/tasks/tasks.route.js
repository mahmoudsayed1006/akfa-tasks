import express from 'express';
import TasksController from '../../controllers/tasks/tasks.controller';
import {requireAuth } from '../../services/passport';
import { multerSaveTo } from '../../services/multer-service';

const router = express.Router();

router.route('/')
    .post(
        requireAuth,
        TasksController.validateBody(),
        TasksController.create
    )
    .get(TasksController.findAll);

router.route('/:taskId')
    .put(
        requireAuth,
        TasksController.validateBody(true),
        TasksController.update
    )
    
    .delete(requireAuth,TasksController.delete);

router.route('/:taskId/task')
    .get(TasksController.findTaskById)
   


router.route('/:taskId/accept')
    .put(
        requireAuth,
        TasksController.accept
    );

router.route('/:taskId/refuse')
    .put(
        requireAuth,
        TasksController.validateReplyBody(),
        TasksController.refuse
    )

router.route('/:taskId/done')
    .put(
        requireAuth,
        multerSaveTo('tasks').array('doneImg',10),
        TasksController.done
    );

router.route('/:taskId/rate')
    .put(
        requireAuth,
        TasksController.rate
    );

router.route('/:userId/:date/monthlyRate')
    .get(
        TasksController.MonthlyRate
    );

router.route('/:taskId/thanks')
    .get(
        requireAuth,
        TasksController.thanks
    );
router.route('/:taskId/changeEmployee')
    .put(
        requireAuth,
        TasksController.updatedTaskEmployee
    )
router.route('/:taskId/Reassignment')
    .put(
        requireAuth,
        TasksController.Reassignment
    )
router.route('/search')
    .post(
        requireAuth,
        TasksController.search
    );
export default router;