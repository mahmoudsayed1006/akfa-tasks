import express from 'express';
import JopController from '../../controllers/jops/jops.controller';
import {requireAuth } from '../../services/passport';

const router = express.Router();

router.route('/')
    .post(
        requireAuth,
        JopController.validateBody(),
        JopController.create
    )
    .get(JopController.findAll);
    

router.route('/:jopId')
    .put(
        requireAuth,
        JopController.validateBody(true),
        JopController.update
    )
    .get(JopController.findById)
    .delete(requireAuth,JopController.delete);

router.route('/search')
    .post(
        requireAuth,
        JopController.search
    );






export default router;