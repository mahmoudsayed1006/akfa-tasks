import express from 'express';
import { requireSignIn, requireAuth } from '../../services/passport';
import { multerSaveTo } from '../../services/multer-service';
import UserController from '../../controllers/user/user.controller';

const router = express.Router();


router.post('/signin', requireSignIn, UserController.signIn);

router.route('/signup')
    .post(
        multerSaveTo('users').single('img'),
        UserController.validateUserCreateBody(),
        UserController.signUp
    );
router.route('/logout')
    .post(
        requireAuth,
        UserController.logout
    );
router.route('/addToken')
    .post(
        requireAuth,
        UserController.addToken
    );
router.route('/updateToken')
    .put(
        requireAuth,
        UserController.updateToken
    );
router.route('/allEmployee')
    .get(UserController.findAllEmployee);

router.route('/search')
    .post(
        requireAuth,
        UserController.search
    );
router.route('/employee/:employeeId')
    .get(UserController.findById);

router.route('/employee/:employeeId')
    .delete( requireAuth,UserController.delete);

router.put('/user/:userId/updateInfo',
    requireAuth,
    multerSaveTo('users').single('img'),
    UserController.validateUpdatedBody(),
    UserController.updateInfo);
router.put('/user/:userId/changePassword',
    requireAuth,
    UserController.validateUpdatedPassword(),
    UserController.updatePassword);

router.put('/check-exist-email', UserController.checkExistEmail);

//router.put('/check-exist-phone', UserController.checkExistPhone);

router.post('/forget-password',
    UserController.validateForgetPassword(),
    UserController.forgetPasswordSms);

router.post('/confirm-code',
    UserController.validateConfirmVerifyCode(),
    UserController.resetPasswordConfirmVerifyCode);

router.post('/reset-password',
    UserController.validateResetPassword(),
    UserController.resetPassword);

export default router;
