var express = require('express');
var router = express.Router();
var messageController = require('../../controllers/message/messageController');

/* GET users listing. */
router.get('/',messageController.getAllMessages);
router.get('/lastContacts',messageController.findLastContacts);
router.put('/',messageController.updateSeen);

module.exports = router;