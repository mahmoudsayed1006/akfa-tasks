import express from 'express';
import NotifController from '../../controllers/notif/notif.controller';
import {requireAuth } from '../../services/passport';
const router = express.Router();

router.route('/')
    .get(requireAuth,NotifController.find);

router.route('/:notifId/read')
    .put(requireAuth,NotifController.read)

router.route('/:notifId/unread')
    .put(requireAuth,NotifController.unread)
export default router;