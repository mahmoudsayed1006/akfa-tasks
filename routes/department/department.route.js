import express from 'express';
import DepartmentController from '../../controllers/department/department.controller';
import {requireAuth } from '../../services/passport';
const router = express.Router();

router.route('/')
    .post(
         requireAuth,
         DepartmentController.validateBody(),
         DepartmentController.create
    )
    .get( DepartmentController.findAll);


router.route('/:departmentId')
    .put(
        requireAuth,
         DepartmentController.validateBody(true),
         DepartmentController.update
    )
    .get( DepartmentController.findById)
    .delete(requireAuth, DepartmentController.delete);

router.route('/search')
    .post(
        requireAuth,
        DepartmentController.search
    );





export default router;