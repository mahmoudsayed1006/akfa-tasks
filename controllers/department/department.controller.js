import ApiResponse from "../../helpers/ApiResponse";
import Department from "../../models/department/department.model";
import User from "../../models/user/user.model";
import { checkExist, checkExistThenGet } from "../../helpers/CheckMethods";
import {checkValidations } from "../shared/shared.controller";
import { body } from "express-validator/check";
import Jop from "../../models/jops/jops.model";

export default {
    async search(req, res, next) {
        try {
            let page = +req.query.page || 1, limit = +req.query.limit || 20;
            let query = {
                $and: [
                    { $or: [ 
                        {name: new RegExp('^'+req.body.search+'$', "i")},
                      ] 
                    },
                    {deleted: false} 
                ]
            };
            let departments = await Department.find(query)
                .sort({ createdAt: -1 })
                .limit(limit)
                .skip((page - 1) * limit);


            const departmentsCount = await Department.count(query);
            const pageCount = Math.ceil(departmentsCount / limit);

            res.send(new ApiResponse(departments, page, pageCount, limit, departmentsCount, req));
        } catch (err) {
            next(err);
        }
    },
    async findAll(req, res, next) {
        try {
            let page = +req.query.page || 1, limit = +req.query.limit || 20;
            let query = { deleted: false };
            let departments = await Department.find(query)
                .sort({ createdAt: -1 })
                .limit(limit)
                .skip((page - 1) * limit);


            const departmentsCount = await Department.count(query);
            const pageCount = Math.ceil(departmentsCount / limit);

            res.send(new ApiResponse(departments, page, pageCount, limit, departmentsCount, req));
        } catch (err) {
            next(err);
        }
    },
    validateBody(isUpdate = false) {
        let validations = [
            body('name').not().isEmpty().withMessage('department name is required')
                .custom(async (val, { req }) => {
                    let query = { name: val, deleted: false };

                    if (isUpdate)
                        query._id = { $ne: req.params.departmentId };

                    let department = await Department.findOne(query).lean();
                    if (department)
                        throw new Error('department duplicated name');

                    return true;
                })
        ];

        return validations;
    },

    async create(req, res, next) {

        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));

            const validatedBody = checkValidations(req);

            let createdDepartment = await Department.create({ ...validatedBody});

           
            res.status(201).send(createdDepartment);
        } catch (err) {
            next(err);
        }
    },


    async findById(req, res, next) {
        try {
            let { departmentId } = req.params;
            await checkExist(departmentId, Department, { deleted: false });
            let department = await Department.findById(departmentId);
            res.send(department);
        } catch (err) {
            next(err);
        }
    },

    async update(req, res, next) {

        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));

            let { departmentId } = req.params;
            await checkExist(departmentId, Department, { deleted: false });

            const validatedBody = checkValidations(req);
            let updatedDepartment = await Department.findByIdAndUpdate(departmentId, {
                ...validatedBody,
            }, { new: true });
            res.status(200).send(updatedDepartment);
        }
        catch (err) {
            next(err);
        }
    },

    async delete(req, res, next) {
        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));
                
            let { departmentId } = req.params;
            let department = await checkExistThenGet(departmentId, Department, { deleted: false });
            let employees = await User.find({ department: departmentId });
            for (let employee of employees) {
                employee.deleted = true;
                await employee.save();
            }
            let jops = await Jop.find({ department: departmentId });
            for (let jop of jops) {
                jop.deleted = true;
                await jop.save();
            }
            department.deleted = true;
            await department.save();
            res.send('delete success');

        } catch (err) {
            next(err);
        }
    }
};