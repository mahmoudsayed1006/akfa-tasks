import ApiResponse from "../../helpers/ApiResponse";
import ApiError from '../../helpers/ApiError';
import User from "../../models/user/user.model";
import Task from "../../models/tasks/tasks.model";
import SubTask from "../../models/subtasks/subtasks.model";
import { checkExist, checkExistThenGet } from "../../helpers/CheckMethods";
import { body } from "express-validator/check";
import { handleImgs, checkValidations } from "../shared/shared.controller";
import Notif from "../../models/notif/notif.model";
import { sendNotifiAndPushNotifi } from "../../services/notification-service";

const populateQuery = [
    { path: 'employee', model: 'user' },
    { path: 'task', model: 'task' },
];

export default {

    async findAll(req, res, next) {

        try {
            let page = +req.query.page || 1, limit = +req.query.limit || 20;
            let query = { deleted: false };
            let subtasks = await SubTask.find(query).populate(populateQuery)
                .sort({ createdAt: -1 })
                .limit(limit)
                .skip((page - 1) * limit);

            const subtasksCount = await SubTask.count(query);
            const pageCount = Math.ceil(subtasksCount / limit);

            res.send(new ApiResponse(subtasks, page, pageCount, limit, subtasksCount, req));
        } catch (err) {
            next(err);
        }
    },
    async findAllInTask(req, res, next) {
        try {
            let page = +req.query.page || 1, limit = +req.query.limit || 20
                , { taskId } = req.params;
            await checkExist(taskId, Task);
            let query = { task: +taskId, deleted: false };
            let subtasks = await SubTask.find(query).populate(populateQuery)
                .sort({ createdAt: -1 })
                .limit(limit)
                .skip((page - 1) * limit);

            const subtasksCount = await SubTask.count(query);
            const pageCount = Math.ceil(subtasksCount / limit);

            res.send(new ApiResponse(subtasks, page, pageCount, limit, subtasksCount, req));
        } catch (err) {
            next(err);
        }
    },
  

    async create(req, res, next) {

        try {
            let user = req.user;
            let createdSubTask = await SubTask.create({ 
                title:req.body.title,
                date:new Date(req.body.date).toISOString(),
                endDate:new Date(req.body.endDate).toISOString(),
                employee:req.body.employee,
                task:req.body.task,
                number:req.body.number,
                notes:req.body.notes
            });
            let task = await checkExistThenGet(req.body.task, Task);
            task.steps.push(createdSubTask._id);
            await task.save();
            res.status(201).send({
                subtask: await SubTask.findOne(createdSubTask).populate(populateQuery),
            });
        } catch (err) {
            next(err);
        }
    },

    async findById(req, res, next) {
        try {
            let { subtaskId } = req.params;
            await checkExist(subtaskId, SubTask, { deleted: false });
            let subtask = await SubTask.findById(subtaskId);
            res.send(subtask);
        } catch (err) {
            next(err);
        }
    },

    async delete(req, res, next) {
        try {
                
            let { subtaskId } = req.params;
            let subtask = await checkExistThenGet(subtaskId, SubTask, { deleted: false });
            let user = req.user;
            if (user.id != subtask.employee)
                return next(new ApiError(403, ('not allowed')));

            subtask.deleted = true;
            await subtask.save();
            res.status(204).send('delete success');

        }
        catch (err) {
            next(err);
        }
    },
    validateBody(isUpdate = false) {
        let validations = [];
        if (isUpdate)
            validations.push([
                body('completeImg').isEmpty().withMessage('img should be a valid img')
            ]);

        return validations;
    },
    async complete(req,res,next){
        try{
            let user = req.user;
            let { subtaskId} = req.params;
            let subtask = await checkExistThenGet(subtaskId, SubTask,
                {deleted: false });
            if (user.id != subtask.employee)
                return next(new ApiError(403, ('not allowed')));
            const validatedBody = checkValidations(req);
            
                let image = await handleImgs(req, { attributeName: 'completeImg', isUpdate: false });
                validatedBody.completeImg = image;
            
            let updateSubtask= await SubTask.findByIdAndUpdate(subtaskId, {
                ...validatedBody,completeImg:image
            }, { new: true });
            subtask.complete = true;
            subtask.save();
            let users = await User.find({'type':'ADMIN'});
            users.forEach(user => {
                sendNotifiAndPushNotifi({
                    targetUser: user.id, 
                    fromUser: req.user, 
                    text: 'new notification',
                    subject: subtask.id,
                    subjectType: 'subtask complete'
                });
                let notif = {
                    "description":'subtask complete'
                }
                Notif.create({...notif,resource:req.user,target:user.id,subject:subtask.task});
            });
            res.send(updateSubtask);
            
        } catch(err){
            next(err);
        }
    }
    
};