import User from "../../models/user/user.model";
import { checkExistThenGet, checkExist} from "../../helpers/CheckMethods";
import Notif from "../../models/notif/notif.model";
import SubTask from "../../models/subtasks/subtasks.model";
import ApiResponse from "../../helpers/ApiResponse";
const populateQuery = [ 
    { path: 'subject', model: 'task' },
    { path: 'resource', model: 'user' },
    { path: 'target', model: 'user' },
    { path: 'subject.steps' ,  model: 'subtask' },
];


export default {
  
    async find(req, res, next) {
        try {
            let user = req.user._id;
            await checkExist(req.user._id, User);
            let page = +req.query.page || 1, limit = +req.query.limit || 20;
            let query = { deleted: false,target:user };
            let notifs = await Notif.find(query).populate(populateQuery)
                .sort({ createdAt: -1 })
                .limit(limit)
                .skip((page - 1) * limit);


            const notifsCount = await Notif.count(query);
            const pageCount = Math.ceil(notifsCount / limit);

            res.send(new ApiResponse(notifs, page, pageCount, limit, notifsCount, req));
        } catch (err) {
            next(err);
        }
    },
    async read(req, res, next) {
        try {
            let { notifId} = req.params;
            let notif = await checkExistThenGet(notifId, Notif);
            notif.read = true;
            await notif.save();
            res.send('notif read');
        } catch (error) {
            next(error);
        }
    },

    async unread(req, res, next) {
        try {
            let { notifId} = req.params;
            let notif = await checkExistThenGet(notifId, Notif);
            notif.read = false;
            await notif.save();
            res.send('notif unread');
        } catch (error) {
            next(error);
        }
    },
}