import ApiResponse from "../../helpers/ApiResponse";
import Department from "../../models/department/department.model";
import User from "../../models/user/user.model";
import Task from "../../models/tasks/tasks.model";
import SubTask from "../../models/subtasks/subtasks.model";
import ApiError from '../../helpers/ApiError';
import { checkExist, checkExistThenGet } from "../../helpers/CheckMethods";
import { handleImgs, checkValidations } from "../shared/shared.controller";
import { body } from "express-validator/check";
import Notif from "../../models/notif/notif.model";
import { sendNotifiAndPushNotifi } from "../../services/notification-service";

const populateQuery = [
    { path: 'employee', model: 'user' },
    { path: 'follower', model: 'user' },
    { path: 'department', model: 'department' },
    { path:'steps', model: 'subtask'}
];

export default {
    async search(req, res, next) {

        try {
            
            let page = +req.query.page || 1, limit = +req.query.limit || 20;
            let query = {
                $and: [
                    { $or: [ 
                        {task: new RegExp('^'+req.body.search+'$', "i")},
                        {details: new RegExp('^'+req.body.search+'$', "i")},
                      ] 
                    },
                    {deleted: false} 
                ]
            };
            let tasks = await Task.find(query).populate(populateQuery)
                .sort({ createdAt: -1 })
                .limit(limit)
                .skip((page - 1) * limit);
            const tasksCount = await Task.count(query);
            const pageCount = Math.ceil(tasksCount / limit);

            res.send(new ApiResponse(tasks, page, pageCount, limit, tasksCount, req));
        } catch (err) {
            next(err);
        }
    },
    async findAll(req, res, next) {

        try {
            
            let page = +req.query.page || 1, limit = +req.query.limit || 20,
            { status, userId, followerId, departmentId ,type} = req.query;
            let query = { deleted: false };
            let {date} = req.query;
            var mydate = new Date(date);
            console.log(mydate);
            var dd = mydate.getDate();
            var mm = mydate.getMonth()+1;
            var yyyy = mydate.getFullYear();
            if(dd<10) {
                dd = '0'+dd
            } 
            if(mm<10) {
                mm = '0'+mm
            } 
            let from = yyyy + '-' + mm + '-' + dd + 'T00:00:00.000Z';
            let to= yyyy + '-' + mm + '-' + dd + 'T23:59:00.000Z';
            if(date) {
                query = { 
                    start: { $gt : new Date(from), $lt : new Date(to) }
               };
            } 

            if (status) query.status = status;
            if (followerId) query.follower = followerId;
            if (userId) query.employee = userId;
            if (departmentId) query.department = departmentId;
            if (type) query.type = type;
            
            console.log(query.start);
            let tasks = await Task.find(query).populate(populateQuery)
                .sort({ createdAt: -1 })
                .limit(limit)
                .skip((page - 1) * limit);
            const tasksCount = await Task.count(query);
            const pageCount = Math.ceil(tasksCount / limit);

            res.send(new ApiResponse(tasks, page, pageCount, limit, tasksCount, req));
        } catch (err) {
            next(err);
        }
    },
  
    
    validateBody(isUpdate = false) {
        let validations = [
            body('task').not().isEmpty().withMessage('task is required'),
            body('type').isIn(['IMPORTANT','NORMAL','URGENT']).withMessage('wrong type'),
            body('start').not().isEmpty().withMessage('start date is required'),
            body('end').not().isEmpty().withMessage('end date is required'),
            body('employee').not().isEmpty().withMessage('employee is required')
            .isNumeric().withMessage('numeric value required'),
            body('department').not().isEmpty().withMessage('department is required')
            .isNumeric().withMessage('numeric value required'),
            body('follower').not().isEmpty().withMessage('follower is required')
            .isNumeric().withMessage('numeric value required'),
        ];

        return validations;
    },

    async create(req, res, next) {

        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));

            const validatedBody = checkValidations(req);

            let createdTask = await Task.create({ ...validatedBody,details:req.body.details});
            sendNotifiAndPushNotifi({
                targetUser: validatedBody.employee, 
                fromUser: req.user, 
                text: 'new notification',
                subject: createdTask.id,
                subjectType: 'new task'
            });
            let notif = {
                "description":validatedBody.task
            }
            Notif.create({...notif,resource:req.user,target:user.id,subject:createdTask.id});
            res.status(201).send({
                task: await Task.findOne(createdTask).populate(populateQuery),
            });
        } catch (err) {
            next(err);
        }
    },


    async findTaskById(req, res, next) {
        try {

            let { taskId } = req.params;
            console.log(taskId);
            await checkExist(taskId, Task, { deleted: false });
            let task = await Task.findById(taskId).populate(populateQuery)
            res.status(200).send({
                task
            });
        } catch (err) {
            next(err);
        }
    },
  
    async update(req, res, next) {

        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));

            let { taskId } = req.params;
            await checkExist(taskId, Task, { deleted: false });

            const validatedBody = checkValidations(req);
            let updatedTask = await Task.findByIdAndUpdate(taskId, {
                ...validatedBody,details:req.body.details
            }, { new: true });
            sendNotifiAndPushNotifi({
                targetUser: updatedTask.employee, 
                fromUser: req.user, 
                text: 'new notification',
                subject: updatedTask.id,
                subjectType: 'new task'
            });
            let notif = {
                "description":updatedTask.task
            }
            Notif.create({...notif,resource:req.user,target:user.id,subject:updatedTask.id});
            res.status(200).send(updatedTask);
        }
        catch (err) {
            next(err);
        }
    },
    async updatedTaskEmployee(req, res, next) {

        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));

            let { taskId } = req.params;
            await checkExist(taskId, Task, { deleted: false });
            const validatedBody = checkValidations(req);
            let updatedTask = await checkExistThenGet(taskId, Task, { delheted: false });
            updatedTask.employee = req.body.employee;
            updatedTask.follower = req.body.follower;
            updatedTask.status = 'INPROGRESS';
            await updatedTask.save();
            sendNotifiAndPushNotifi({
                targetUser: updatedTask.employee, 
                fromUser: req.user, 
                text: 'new notification',
                subject: updatedTask.id,
                subjectType: 'new task'
            });
            let notif = {
                "description":updatedTask.task
            }
            Notif.create({...notif,resource:req.user,target:user.id,subject:updatedTask.id});
            res.status(200).send(updatedTask);
        }
        catch (err) {
            next(err);
        }
    },
    async Reassignment(req, res, next) {

        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));

            let { taskId } = req.params;
            await checkExist(taskId, Task, { deleted: false });
            let updatedTask = await checkExistThenGet(taskId, Task, { deleted: false });
            updatedTask.status = 'INPROGRESS';
            await updatedTask.save();
            sendNotifiAndPushNotifi({
                targetUser: updatedTask.employee, 
                fromUser: req.user, 
                text: 'new notification',
                subject: updatedTask.id,
                subjectType: 'new task'
            });
            let notif = {
                "description":updatedTask.task
            }
            Notif.create({...notif,resource:req.user,target:user.id,subject:updatedTask.id});
            res.status(200).send(updatedTask);
        }
        catch (err) {
            next(err);
        }
    },

    async delete(req, res, next) {
        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));
                
            let { taskId } = req.params;
            let task = await checkExistThenGet(taskId, Task, { deleted: false });
            task.deleted = true;
            await task.save();
            res.status(204).send('delete success');

        }
        catch (err) {
            next(err);
        }
    },
    
    async accept(req, res, next) {
        try {

            let { taskId} = req.params;
            let task = await checkExistThenGet(taskId, Task);
            task.status = 'INPROGRESS';
            await task.save();
            res.send('accept task');
            let users = await User.find({'type':'ADMIN'});
            users.forEach(user => {
                sendNotifiAndPushNotifi({
                    targetUser: user.id, 
                    fromUser: req.user, 
                    text: 'new notification',
                    subject: task.id,
                    subjectType: 'task accept'
                });
                let notif = {
                    "description":'task accept'
                }
                Notif.create({...notif,resource:req.user,target:user.id,subject:task.id});
            });
        } catch (error) {
            next(error);
        }
    },
    validateReplyBody() {
        let validation = [
            body('reply').not().isEmpty().withMessage('reply required')
        ]
        return validation;
    },
    async refuse(req, res, next) {
        try {

            let { taskId,} = req.params;
            let task = await checkExistThenGet(taskId, Task);
            task.status = 'REJECTED';
            const validatedBody = checkValidations(req);
            task.reply =  validatedBody.reply;
            await task.save();
            res.send('refuse task');
            let users = await User.find({'type':'ADMIN'});
            users.forEach(user => {
                sendNotifiAndPushNotifi({
                    targetUser: user.id, 
                    fromUser: req.user, 
                    text: 'new notification',
                    subject: task.id,
                    subjectType: 'task refuse'
                });
                let notif = {
                    "description":'task refuse'
                }
                Notif.create({...notif,resource:req.user,target:user.id,subject:task.id});
            });
        } catch (error) {
            next(error);
        }
    },
    async done(req,res,next){
        try{
            let user = req.user;
            let { taskId} = req.params;
            let task = await checkExistThenGet(taskId, Task,
                {deleted: false });
            if (user.id != task.employee)
                return next(new ApiError(403, ('not allowed')));

            const validatedBody = checkValidations(req);
            if (req.file) {
                validatedBody.img = await handleImgs(req, { isUpdate: true, attributeName: 'doneImg' });
            }
            task.status = 'FINSHED';
            task.doneDate = req.body.doneDate;
            console.log(task.doneDate);
            task.doneImg = validatedBody.img;
            await task.save();
            let users = await User.find({'type':'ADMIN'});
            users.forEach(user => {
                sendNotifiAndPushNotifi({
                    targetUser: user.id, 
                    fromUser: req.user, 
                    text: 'new notification',
                    subject: task.id,
                    subjectType: 'task complete'
                });
                let notif = {
                    "description":'task complete'
                }
                Notif.create({...notif,resource:req.user,target:user.id,subject:task.id});
            });
            res.send('done');
            
        } catch(err){
            next(err);
        }
    },
    async rate(req,res,next){
        try{
            let user = req.user;
            let { taskId} = req.params;
            let task = await checkExistThenGet(taskId, Task,
                {deleted: false });
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('not allowed')));
            
            task.rate =req.body.rate;
            await task.save();
            
            res.send('rate send');
            
        } catch(err){
            next(err);
        }
    },
    async MonthlyRate(req,res,next){
        try{
            let { userId} = req.params;
            let {date} = req.params;
            var mydate = new Date(date);
            console.log(mydate);
            var dd = mydate.getDate();
            var mm = mydate.getMonth()+1;
            var yyyy = mydate.getFullYear();
            if(dd<10) {
                dd = '0'+dd
            } 
            if(mm<10) {
                mm = '0'+mm
            } 
            let from = yyyy + '-' + mm + '-' + dd + 'T00:00:00.000Z';

            
            var nextdate = new Date(date);
            var dd = nextdate.getDate();
            var nextMM = nextdate.getMonth()+2;
            var nextYYYY = nextdate.getFullYear();
            if(dd<10) {
                dd = '0'+dd
            } 
            if(nextMM<10) {
                nextMM = '0'+nextMM
            } 
            if(nextMM > 12){
                nextMM = '0'+1 ;
                nextYYYY = yyyy + 1;
            }
            let to = nextYYYY + '-' + nextMM + '-' + dd + 'T23:59:59.000Z';
            console.log(to);
            let query = { 
                $and: [
                    {doneDate: { $gt : new Date(from), $lt : new Date(to) }}, 
                    {employee:userId},
                    {status:'FINSHED'},
                    {deleted: false} 
                ]
           };
          
            let tasks = await Task.find(query).select('rate');
            var arr = [];
            for (var prop in tasks) {
                arr.push(tasks[prop]);
            }
            var sum = 0;
            for(var i = 0; i < arr.length; i++){
            sum += arr[i].rate
            }
            let totalRates = arr.length * 5;
            let MonthlyRate = sum *100 / totalRates;
            console.log(MonthlyRate);
            let employee = await checkExistThenGet(userId, User,
                {deleted: false });
            employee.MonthlyRate = MonthlyRate;
            await employee.save();
            res.status(200).send({
                tasks,
                MonthlyRate
            });
        } catch(err){
            next(err);
        }
    },

    
    async thanks(req,res,next){
        try{
            let user = req.user;
            let { taskId} = req.params;
            let task = await checkExistThenGet(taskId, Task,
                {deleted: false });
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin auth')));
            let end = Number(new Date(task.end));
            console.log(end);
            let done = Number(new Date(task.doneDate));
            const date = done - end;
            console.log(date);
            function convertMS( milliseconds ) {
                var day, hour, minute, seconds;
                seconds = Math.floor(milliseconds / 1000);
                minute = Math.floor(seconds / 60);
                seconds = seconds % 60;
                hour = Math.floor(minute / 60);
                minute = minute % 60;
                day = Math.floor(hour / 24);
                hour = hour % 24;
                return day;
            }
            let days = convertMS(date);
            if(date > 0){
                sendNotifiAndPushNotifi({
                    targetUser: task.employee, 
                    fromUser: req.user, 
                    text: 'new notification',
                    subject: task.id,
                    subjectType:  'يوم '+ days +' شكرا علما انك تاخرت عن الموعد المحدد'
                });
                let notif = {
                    "description": 'يوم '+ days +' شكرا علما انك تاخرت عن الموعد المحدد'
                }
                Notif.create({...notif,resource:req.user,target:task.employee,subject:task.id});
                
                res.send('late');
            } else{
                    sendNotifiAndPushNotifi({
                        targetUser: user.id, 
                        fromUser: req.user, 
                        text: 'new notification',
                        subject: task.id,
                        subjectType: 'شكرا على انجاز المهمه فى الوقت المحدد'
                    });
                    let notif = {
                        "description":'شكرا على انجاز المهمه فى الوقت المحدد'
                    }
                    Notif.create({...notif,resource:req.user,target:user.id,subject:task.id});
                res.send('unlate');
            }
            
            
        } catch(err){
            next(err);
        }
    }
};