import ApiResponse from "../../helpers/ApiResponse";
import Archive from "../../models/archive/archive.model";
import { checkExist, checkExistThenGet } from "../../helpers/CheckMethods";
import { body } from "express-validator/check";
import { handleImgs, checkValidations } from "../shared/shared.controller";

export default {
    async search(req, res, next) {

        try {
            let page = +req.query.page || 1, limit = +req.query.limit || 20;
            let query = {
                $and: [
                    { $or: [ 
                        {description: new RegExp('^'+req.body.search+'$', "i")},
                      ] 
                    },
                    {deleted: false} 
                ]
            };
            let archives = await Archive.find(query).populate('user')
                .sort({ createdAt: -1 })
                .limit(limit)
                .skip((page - 1) * limit);

            const archivesCount = await Archive.count(query);
            const pageCount = Math.ceil(archivesCount / limit);

            res.send(new ApiResponse(archives, page, pageCount, limit, archivesCount, req));
        } catch (err) {
            next(err);
        }
    },
    async findAll(req, res, next) {

        try {
            let page = +req.query.page || 1, limit = +req.query.limit || 20;
            let {userId} = req.params;
            let query = { deleted: false,user:userId };
            let archives = await Archive.find(query).populate('user')
                .sort({ createdAt: -1 })
                .limit(limit)
                .skip((page - 1) * limit);

            const archivesCount = await Archive.count(query);
            const pageCount = Math.ceil(archivesCount / limit);

            res.send(new ApiResponse(archives, page, pageCount, limit, archivesCount, req));
        } catch (err) {
            next(err);
        }
    },
    validateBody(isUpdate = false) {
        let validations = [
            body('description').not().isEmpty().withMessage('description is required')
               
        ];

        return validations;
    },

    async create(req, res, next) {

        try {
            let user = req.user;
            let img = await handleImgs(req);
            const validatedBody = checkValidations(req);

            let createdArchive = await Archive.create({ ...validatedBody, files: img, user:user});
            res.status(201).send(createdArchive);
        } catch (err) {
            next(err);
        }
    },


    async findById(req, res, next) {
        try {
            let { archiveId } = req.params;
            await checkExist(archiveId, Archive, { deleted: false });
            let archive = await Archive.findById(archiveId);
            res.send(archive);
        } catch (err) {
            next(err);
        }
    },

    async update(req, res, next) {

        try {
            let user = req.user;
            let { archiveId } = req.params;
            const validatedBody = checkValidations(req);
            if (req.file) {
                validatedBody.files = await handleImgs(req, { isUpdate: true, attributeName: 'files' });
            }

            let updatedArchive = await Archive.findByIdAndUpdate(archiveId, {
                ...validatedBody,

            }, { new: true });

            res.status(200).send(updatedArchive);
        }
        catch (err) {
            next(err);
        }
    },

    async delete(req, res, next) {
        try {
            let user = req.user;
            let { archiveId } = req.params;
            let archive = await checkExistThenGet(archiveId, Archive, { deleted: false });
            archive.deleted = true;
            await archive.save();
            res.status(204).send('delete success');

        }
        catch (err) {
            next(err);
        }
    },
    
};