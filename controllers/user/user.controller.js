import { checkExistThenGet, checkExist } from '../../helpers/CheckMethods';
import { body } from 'express-validator/check';
import { checkValidations, handleImg } from '../shared/shared.controller';
import { generateToken } from '../../utils/token';
import User from "../../models/user/user.model";
import Department from "../../models/department/department.model";
import Jop from "../../models/jops/jops.model";
import bcrypt from 'bcryptjs';
import ApiResponse from "../../helpers/ApiResponse";
import ApiError from '../../helpers/ApiError';
import config from '../../config';
import { sendNotifiAndPushNotifi } from "../../services/notification-service";
import { sendForgetPassword } from '../../services/message-service';
import { generateVerifyCode } from '../../services/generator-code-service';
const checkUserExistByPhone = async (phone) => {
    let user = await User.findOne({ phone });
    if (!user)
        throw new ApiError.BadRequest('Phone Not Found');

    return user;
}
const populateQuery = [
    { path: 'jop', model: 'jop' },
    { path: 'department', model: 'department' },
];
export default {
    
    async addToken(req,res,next){
        try{
            let user = req.user;
            let users = await checkExistThenGet(user.id, User);
            let arr = users.token;
            var found = arr.find(function(element) {
                return element == req.body.token;
            });
            if(!found){
                users.token.push(req.body.token);
                await users.save();
            console.log(req.body.token);
            }
            res.status(200).send({
                users,
            });
            
        } catch(err){
            next(err);
        }
    },
    async signIn(req, res, next) {
        try{
            let user = req.user;
            user = await User.findById(user.id).populate(populateQuery),
                res.status(200).send({
                    user,
                    token: generateToken(user.id)
                });
        } catch(err){
            next(err);
        }
    },

    validateUserCreateBody(isUpdate = false) {
        let validations = [
            body('name').not().isEmpty().withMessage('name is required'),
            body('jop').not().isEmpty().withMessage('jop is required')
            .isNumeric().withMessage('numeric value required'),
            body('department').not().isEmpty().withMessage('department is required')
            .isNumeric().withMessage('numeric value required'),
            body('phone').not().isEmpty().withMessage('phone is required')
                .custom(async (value, { req }) => {
                    let userQuery = { phone: value };
                    if (isUpdate && req.user.phone === value)
                        userQuery._id = { $ne: req.user._id };

                    if (await User.findOne(userQuery))
                        throw new Error(req.__('phone duplicated'));
                    else
                        return true;
                }),
            body('emergencyNumber'),
            body('email').not().isEmpty().withMessage('email is required')
                .isEmail().withMessage('email syntax')
                .custom(async (value, { req }) => {
                    let userQuery = { email: value };
                    if (isUpdate && req.user.email === value)
                        userQuery._id = { $ne: req.user._id };

                    if (await User.findOne(userQuery))
                        throw new Error(req.__('email duplicated'));
                    else
                        return true;
                }),

                body('type').not().isEmpty().withMessage('type is required')
                .isIn(['EMPLOYEE','SUB-ADMIN', 'ADMIN']).withMessage('wrong type'),
            
        ];
        if (!isUpdate) {
            validations.push([
                body('password').not().isEmpty().withMessage('password is required'),
                body('img').optional().custom(val => isImgUrl(val)).withMessage('img should be a valid img')
            ]);
        }
        return validations;
    },
    
    async signUp(req, res, next) {
        try {
            let userName = req.body.username;  
            const validatedBody = checkValidations(req);
            let image;
            if (req.file) {
                image = await handleImg(req, { attributeName: 'img'});
                validatedBody.img = image;
            }
            let createdUser;
            if(validatedBody.img){
                createdUser = await User.create({
                    ...validatedBody,username:userName,img:image
                });
            }
            else{
                createdUser = await User.create({
                    ...validatedBody,username:userName
                });
            }
            
            res.status(201).send({
                user: await User.findOne(createdUser).populate(populateQuery),
                token: generateToken(createdUser.id)
            });

        } catch (err) {
            next(err);
        }
    },
    
    async findById(req, res, next) {
        try {
            let { id } = req.params;
            await checkExist(id, User, { deleted: false });

            let user = await User.findById(id).populate(populateQuery);
            res.send(user);
        } catch (error) {
            next(error);
        }
    },

    validateUpdatedBody(isUpdate = true) {
        let validation = [
            body('name'),
            body('jop'),
            body('department'),
            body('phone')
            .custom(async (value, { req }) => {
                let userQuery = { phone: value };
                let {userId} = req.params;
                let user = await checkExistThenGet(userId, User);
                if (isUpdate && user.phone === value)
                    userQuery._id = { $ne: userId};

                if (await User.findOne(userQuery))
                    throw new Error(req.__('phone duplicated'));
                else
                    return true;
            }),
            body('emergencyNumber'),
            body('email')
                .custom(async (value, { req }) => {
                    let {userId} = req.params;
                    let user = await checkExistThenGet(userId, User);
                    let userQuery = { email: value };
                    if (isUpdate && user.email === value)
                        userQuery._id = { $ne: userId };

                    if (await User.findOne(userQuery))
                        throw new Error(req.__('email duplicated'));
                    else
                        return true;
            }),

        ];
        if (isUpdate)
            validation.push([
                body('img').optional().custom(val => isImgUrl(val)).withMessage('img should be a valid img')
            ]);

        return validation;
    },
    
    
    async updateInfo(req, res, next) {
        try {
            const validatedBody = checkValidations(req);
            let {userId} = req.params;
            let user = await checkExistThenGet(userId, User);
            if (req.file) {
                let image = await handleImg(req, { attributeName: 'img', isUpdate: true });
                validatedBody.img = image;
            }

            if(validatedBody.emergencyNumber){
                 user.emergencyNumber = validatedBody.emergencyNumber;
            }
            if(validatedBody.phone){
                user.phone = validatedBody.phone;
            }
            if(validatedBody.name){
                user.name = validatedBody.name;
            }
            if(validatedBody.department){
                user.department = validatedBody.department;
            }
            if(validatedBody.jop){
                user.jop = validatedBody.jop;
            }
            if(validatedBody.img){
                user.img = validatedBody.img;
            }
            await user.save();
            res.status(200).send({
                user: await User.findOne(user).populate(populateQuery),
            });

        } catch (error) {
            next(error);
        }
    },

    validateUpdatedPassword(isUpdate = false) {
        let validation = [
            body('newPassword').optional().not().isEmpty().withMessage('newPassword is required'),
            body('currentPassword').optional().not().isEmpty().withMessage('currentPassword is required')
           
        ];

        return validation;
    },
    async updatePassword(req, res, next) {
        try {
            let {userId} = req.params;
            let user = await checkExistThenGet(userId, User);
            if (req.body.newPassword) {
                if (req.body.currentPassword) {
                    if (bcrypt.compareSync(req.body.currentPassword, user.password)) {
                        user.password = req.body.newPassword;
                    }
                    else {
                        res.status(400).send({
                            error: [
                                {
                                    location: 'body',
                                    param: 'currentPassword',
                                    msg: 'currentPassword is invalid'
                                }
                            ]
                        });
                    }
                }
            }
            await user.save();
            res.status(200).send({
                user: await User.findById(userId)
            });

        } catch (error) {
            next(error);
        }
    },
    async findById(req, res, next) {
        try {
            let { employeeId } = req.params;
            await checkExist(employeeId, User, { deleted: false,type:'EMPLOYEE'  });
            let employee = await User.findById(employeeId).populate(populateQuery)
            res.send(employee);
        } catch (err) {
            next(err);
        }
    },
    async delete(req, res, next) {
        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));
                
            let { employeeId } = req.params;
            let employee = await checkExistThenGet(employeeId, User, { deleted: false });
            employee.deleted = true;
            await employee.save();
            res.status(204).send('delete success');

        }
        catch (err) {
            next(err);
        }
    },
    async search(req, res, next) {

        try {
            let page = +req.query.page || 1, limit = +req.query.limit || 20;
            let query = {
                $and: [
                    { $or: [ 
                        {name: new RegExp('^'+req.body.search+'$', "i")},
                        {email: new RegExp('^'+req.body.search+'$', "i")},
                        {phone: new RegExp('^'+req.body.search+'$', "i")},
                        {username: new RegExp('^'+req.body.search+'$', "i")},
                      ] 
                    },
                    {deleted: false} 
                ]
            };
            employees = await User.find(query).populate(populateQuery)
            .sort({ createdAt: -1 })
            .limit(limit)
            .skip((page - 1) * limit);
            
            const employeesCount = await User.count(query);
            const pageCount = Math.ceil(employeesCount / limit);

            res.send(new ApiResponse(employees, page, pageCount, limit, employeesCount, req));
        } catch (err) {
            next(err);
        }
    },
    async findAllEmployee(req, res, next) {

        try {
            let page = +req.query.page || 1, limit = +req.query.limit || 20,
            {sort, departmentId,jopId } = req.query;
            let query = { deleted: false,type:'EMPLOYEE' };
            if (departmentId) query.department = departmentId;
            if (jopId) query.jop = jopId;
            let employees;
            if(sort == "up"){
                 employees = await User.find(query).populate(populateQuery)
                .sort({ MonthlyRate: -1 })
                .limit(limit)
                .skip((page - 1) * limit);
            }
            if(sort == "down"){
                employees = await User.find(query).populate(populateQuery)
                .sort({ MonthlyRate: 1 })
                .limit(limit)
                .skip((page - 1) * limit);
            }
            if(sort == undefined){
                employees = await User.find(query).populate(populateQuery)
                .sort({ createdAt: -1 })
                .limit(limit)
                .skip((page - 1) * limit);
            }
            const employeesCount = await User.count(query);
            const pageCount = Math.ceil(employeesCount / limit);

            res.send(new ApiResponse(employees, page, pageCount, limit, employeesCount, req));
        } catch (err) {
            next(err);
        }
    },
    async updateToken(req,res,next){
        try{
            let users = await checkExistThenGet(req.user._id, User);
            let arr = users.token;
            var found = arr.find(function(element) {
                return element == req.body.newToken;
            });
            if(!found){
                users.token.push(req.body.newToken);
                await users.save();
            }
            let oldtoken = req.body.oldToken;
            console.log(arr);
            for(let i = 0;i<= arr.length;i=i+1){
                if(arr[i] == oldtoken){
                    arr.splice(arr[i], 1);
                }
            }
            users.token = arr;
            await users.save();
            res.status(200).send(await checkExistThenGet(req.user._id, User));
        } catch(err){
            next(err)
        }
    },
    async logout(req,res,next){
        try{
            let users = await checkExistThenGet(req.user._id, User);
            let arr = users.token;
            let token = req.body.token;
            console.log(arr);
            for(let i = 0;i<= arr.length;i=i+1){
                if(arr[i] == token){
                    arr.splice(arr[i], 1);
                }
            }
            users.token = arr;
            await users.save();
            res.status(200).send(await checkExistThenGet(req.user._id, User));
        } catch(err){
            next(err)
        }
    },
    
    async checkExistEmail(req, res, next) {
        try {
            let email = req.body.email;
            if (!email) {
                return next(new ApiError(400, 'email is required'));
            }
            let exist = await User.findOne({ email: email });
            let duplicated;
            if (exist == null) {
                duplicated = false;
            } else {
                duplicated = true
            }
            return res.status(200).send({ 'duplicated': duplicated });
        } catch (error) {
            next(error);
        }
    },
    validateForgetPassword() {
        return [
            body('phone').not().isEmpty().withMessage('Phone Required')
        ];
    },
    async forgetPasswordSms(req, res, next) {
        try {
            let validatedBody = checkValidations(req);
            let realPhone = config.countrykey + validatedBody.phone;
            let user = await checkUserExistByPhone(validatedBody.phone);

            user.verifycode = generateVerifyCode();
            await user.save();
            //send sms
            sendForgetPassword(user.verifycode, realPhone);
            res.status(204).send();
        } catch (error) {
            next(error);
        }
    },
    validateConfirmVerifyCode() {
        return [
            body('phone').not().isEmpty().withMessage('Phone Required'),
            body('verifycode').not().isEmpty().withMessage('verifycode Required'),
        ];
    },
    async resetPasswordConfirmVerifyCode(req, res, next) {
        try {
            let validatedBody = checkValidations(req);
            let user = await checkUserExistByPhone(validatedBody.phone);
            if (user.verifycode != validatedBody.verifycode)
                return next(new ApiError.BadRequest('verifyCode not match'));
            res.status(204).send();
        } catch (err) {
            next(err);
        }
    },

    validateResetPassword() {
        return [
            body('phone').not().isEmpty().withMessage('phone is required'),
            body('newPassword').not().isEmpty().withMessage('newPassword is required')
        ];
    },

    async resetPassword(req, res, next) {
        try {

            let validatedBody = checkValidations(req);
            let user = await checkUserExistByPhone(validatedBody.phone);

            user.password = validatedBody.newPassword;
            user.verifyCode = '0000';
            await user.save();
            res.status(204).send();

        } catch (err) {
            next(err);
        }
    }



};
