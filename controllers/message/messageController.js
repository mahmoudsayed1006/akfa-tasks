var Message = require('../../models/message/message.model');
var config = require('../../config');
import ApiResponse from "../../helpers/ApiResponse";
var messageController = {

    addnewMessage(io,nsp, data) {
        var toRoom = 'room-' + data.toId;
        var fromRoom ='room-' + data.data.user._id;

        console.log('new message to room ' + toRoom);
        var messData = {
            to: data.toId,
            from: data.data.user._id,
            sent: true
        }
        if (data.data.image != null) {
            messData.image = data.data.image;
        }
        if (data.data.text != null) {
            messData.content = data.data.text;
        }
        /*
        if (io.nsps['/chat'].adapter.rooms[toRoom]) {
            messData.delivered = true;
        }*/
        var query1 = {
            to: data.toId,
            from: data.data.user._id,
            lastMessage: true,
            deleted: false
        }
        var query2 = {
            to: data.data.user._id,
            from: data.toId,
            lastMessage: true,
            deleted: false
        }
        
        Message.updateMany({ $or: [query1, query2] }, {lastMessage: false})
            .then((result1) => {
                if (io.nsps['/chat'].adapter.rooms[toRoom]) {
                    messData.delivered = true;
                }
                var message = new Message(messData);
                message.save()
                    .then(result2 => {
                        console.log(data.data);
                        console.log(result2);
                        nsp.to(toRoom).emit('newMessage', data.data);
                        nsp.to(fromRoom).emit('done',{friendId : data.toId});
                        if (io.nsps['/chat'].adapter.rooms[toRoom]) {
                            console.log("friend is online ");
                            nsp.to(fromRoom).emit('delivered',{friendId : data.toId});
                        }
                    })
                    .catch(err => {
                        console.log('can not save the message .')
                        console.log(err);
                    });
            }).catch((err) => {
                console.log('can not update Last Message.');
                console.log(err);
            });
    },
    async getAllMessages(req, res, next) {
        var page = +req.query.page || config.PAGE;
        var limit = +req.query.limit || config.LIMIT;
        //var myId =  req.user.id || 0 ;
        var myId = +req.query.userId || 0;
        var friendId = +req.query.friendId || 0;
        var query1 = {
            to: myId,
            from: friendId,
            deleted: false
        };
        var query2 = {
            to: friendId,
            from: myId
        }
        Message.find({ $or: [query1, query2] })
            .limit(limit)
            .skip((page - 1) * limit)
            .populate('to from')
            .sort({ _id: -1 })
            .then(async data => {
                var newdata = [] ;
                //res.status(200).send(data);
                data.map(function(element){
                    newdata.push({
                        seen:element.seen,
                        _id : element._id  , 
                        text : element.content , 
                        createdAt : element.incommingDate , 
                        user : {
                           _id :  element.from._id , 
                           name : element.from.username ,
                           avatar: element.from.img
                        }
                    }) ; 
                })
                const messagesCount = await Message.find({ $or: [query1, query2] }).count();
                const pageCount = Math.ceil(messagesCount / limit);
                res.send(new ApiResponse(newdata, page, pageCount, limit, messagesCount, req));
            })
            .catch(err => {
                next(err);
            });
    },

    updateSeen(nsp, data) {
        //var myId = +req.query.userId || 0 ;
        //var friendId = +req.query.friendId || 0;
        var myId = data.myId || 0;
        var friendId = data.toId || 0;
        var toRoom = 'room-' + friendId;
        var query1 = {
            to: myId,
            from: friendId,
            seen: false
        };
        Message.updateMany(query1, { seen: true, seendate: Date.now() })
            .exec()
            .then((result) => {
                nsp.to(toRoom).emit('seen', { friendId: myId });
                console.log("updated");
            })
            .catch((err) => {
                console.log(err);
            });
    },
    async findLastContacts(req, res, next) {
        try {
            var id = +req.query.id || 0;
            var limit = +req.query.limit || 20;
            var page = +req.query.page || 1;
            var query1 = {
                deleted: false,
                lastMessage: true,
                to: id
            }
            var query2 = {
                deleted: false,
                lastMessage: true,
                from: id
            }
            Message.find({ $or: [query1, query2] })
                .sort({ _id: -1 })
                .skip((page - 1) * limit)
                .limit(limit)
                .populate('to from')
                .then(async(data) => {

                    const messagesCount = await Message.find({ $or: [query1, query2] }).count();
                    const pageCount = Math.ceil(messagesCount / limit);
                    var data1=[];
                    var unseenCount = 0;
                    var queryCount = {
                        deleted: false,
                        to: id,
                        seen: false
                    }
                    data1 = await Promise.all( data.map(async (element)=> {
                        if(element.from._id  === id ){
                            queryCount.from = element.to._id; 
                        }else{
                            queryCount.from = element.from._id ; 
                        } 
                        unseenCount = await Message.count(queryCount);
                        element={
                            seen : element.seen,
                            incommingDate: element.incommingDate,
                            lastMessage: element.lastMessage,
                            _id: element.id,
                            to : element.to,
                            from : element.from,
                            content :element.content,
                            unseenCount : unseenCount
                        };
                        return element;
                    }));
                    res.send(new ApiResponse(data1, page, pageCount, limit, messagesCount, req));
                })

        } catch (err) {
            next(err);
        };
    }
};

module.exports = messageController;
