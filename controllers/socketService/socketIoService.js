var Message = require('../models/message/message.model');
module.exports = {

    startChat : function (io) {
        var nsp = io.of('/chat');

        nsp.on('connection', function (socket) { 
            var myId = socket.handshake.query.id;
            var roomName = 'room-' + myId;
            socket.join(roomName);
            console.log('client '+myId+' connected.') 

            nsp.broadcast.emit('connect',{data : "hhhhhhhh"});
            socket.on('newMessage', function (data) {
                var toRoom = 'room-' + data.toId;
                console.log('new message to room '+toRoom )
                var messData = {
                    to: data.toId,
                    from: data.fromId,
                    content: data.message
                }
                var message = new Message(messData);
                var query1 = {
                    to: data.toId,
                    from: data.fromId,
                    lastMessage: true,
                    deleted: false
                }
                var query2 = {
                    to: data.fromId,
                    from: data.toId ,
                    lastMessage: true,
                    deleted: false
                }
                Message.findOneAndUpdate({$or:[query1 , query2]}, { lastMessage: false })
                    .then((result1) => {
                        message.save()
                            .then(result2 => {
                                nsp.to(toRoom).emit('newMessage', data);
                            })
                            .catch(err => {
                                console.log('can not save the message .')
                                console.log(err);
                            });
                    }).catch((err)=>{
                        console.log('can not update Last Message.');
                        console.log(err);
                    });
            });

            /* the data content is : 
             {
               message: this.message,
               toId: this.selected.id,
               fromId: this.id,
               incommingDate : Date.now()
             }
           */
            socket.on('typing', function (data) {
                var toRoom = 'room-' + data.toId;
                nsp.to(toRoom).emit('typing', data);
            });
            
            socket.on('stopTyping', function (data) {
                var toRoom = 'room-' + data.toId;
                nsp.to(toRoom).emit('stopTyping', data);
            });
            /*
            socket.on('uploaded',function(data){
                var toRoom = 'room-' + data.toId;
                nsp.to(toRoom).emit('uploaded',data);
            })*/

            socket.on('disconnect', function () {
                console.log('client disconnected');
            });

        });
    }
}