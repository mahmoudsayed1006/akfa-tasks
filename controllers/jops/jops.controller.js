import ApiResponse from "../../helpers/ApiResponse";
import User from "../../models/user/user.model";
import Jop from "../../models/jops/jops.model";
import { checkExist, checkExistThenGet, isImgUrl } from "../../helpers/CheckMethods";
import {checkValidations } from "../shared/shared.controller";
import { body } from "express-validator/check";
export default {
    async search(req, res, next) {

        try {
            let page = +req.query.page || 1, limit = +req.query.limit || 20;
            let query = {
                $and: [
                    { $or: [ 
                        {name: new RegExp('^'+req.body.search+'$', "i")},
                      ] 
                    },
                    {deleted: false} 
                ]
            };
            let jops = await Jop.find(query)
                .sort({ createdAt: -1 })
                .limit(limit)
                .skip((page - 1) * limit);


            const jopsCount = await Jop.count(query);
            const pageCount = Math.ceil(jopsCount / limit);

            res.send(new ApiResponse(jops, page, pageCount, limit, jopsCount, req));
        } catch (err) {
            next(err);
        }
    },
    async findAll(req, res, next) {

        try {
            let page = +req.query.page || 1, limit = +req.query.limit || 20,
            { departmentId } = req.query;
            let query = { deleted: false };
            if (departmentId) query.department = departmentId;
            let jops = await Jop.find(query)
                .sort({ createdAt: -1 })
                .limit(limit)
                .skip((page - 1) * limit);


            const jopsCount = await Jop.count(query);
            const pageCount = Math.ceil(jopsCount / limit);

            res.send(new ApiResponse(jops, page, pageCount, limit, jopsCount, req));
        } catch (err) {
            next(err);
        }
    },

    validateBody(isUpdate = false) {
        let validations = [
            body('name').not().isEmpty().withMessage('jop name is required')
                .custom(async (val, { req }) => {
                    let query = { name: val, deleted: false };

                    if (isUpdate)
                        query._id = { $ne: req.params.jopId };

                    let jop = await Jop.findOne(query).lean();
                    if (jop)
                        throw new Error('jop duplicated name');

                    return true;
                }),
            body('department').not().isEmpty().withMessage('department is required')
            .isNumeric().withMessage('numeric value required'),
        ];

        return validations;
    },

    async create(req, res, next) {

        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));

            const validatedBody = checkValidations(req);

            let createdjop = await Jop.create({ ...validatedBody});

            res.status(201).send(createdjop);
        } catch (err) {
            next(err);
        }
    },


    async findById(req, res, next) {
        try {
            let { jopId } = req.params;
            await checkExist(jopId, Jop, { deleted: false });
            let jop = await Jop.findById(jopId);
            res.send(jop);
        } catch (err) {
            next(err);
        }
    },

    async update(req, res, next) {

        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));

            let { jopId } = req.params;
            await checkExist(jopId, Jop, { deleted: false });

            const validatedBody = checkValidations(req);
            let updatedJop = await Jop.findByIdAndUpdate(jopId, {
                ...validatedBody,
            }, { new: true });
            
            res.status(200).send(updatedJop);
        }
        catch (err) {
            next(err);
        }
    },
    async delete(req, res, next) {
        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));
                
            let { jopId } = req.params;
            let jop = await checkExistThenGet(jopId, Jop, { deleted: false });
            let employees = await User.find({ jop: jopId });
            for (let employee of employees) {
                employee.deleted = true;
                await employee.save();
            }
            jop.deleted = true;
            await jop.save();
            res.send('delete success');

        } catch (err) {
            next(err);
        }
    }
};